#!/bin/ksh

set -eu

export MAKE=gmake

export AUTOCONF_VERSION=2.71 AUTOMAKE_VERSION=1.16 \
 CONF_CC_OPTS_STAGE1='-Wl,--no-execute-only -Qunused-arguments' \
 CONF_CC_OPTS_STAGE2='-Wl,--no-execute-only -Qunused-arguments'

./boot
./configure --with-ffi-includes=/usr/local/include \
	    --with-ffi-libraries=/usr/local/lib \
 	    --with-gmp-includes=/usr/local/include \
 	    --with-gmp-libraries=/usr/local/lib \
 	    --with-iconv-includes=/usr/local/include \
 	    --with-iconv-libraries=/usr/local/lib \
 	    --with-system-libffi

ulimit -d $((10<<20))
# T4038 needs a bunch of stack
ulimit -s 8192
# quick-validate test
#  -j --docs=none --flavour=default --test-speed=slow test
hadrian/build '*.*.ghc.link.opts+=-optl-Wl,--no-execute-only' \
	      '*.*.ghc.link.opts+=-optl-Qunused-arguments' \
	      '*.*.ghc.link.opts+=-L/usr/local/lib' \
	      --docs=no-sphinx-html \
	      --flavour=release --prefix=/home/greg/ghc-9.6 \
              -j test
