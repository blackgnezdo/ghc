#!/bin/ksh

ulimit -d $((10<<20))
ulimit -s 8192  # T4038 needs a bunch of stack

AUTOCONF_VERSION=2.71 AUTOMAKE_VERSION=1.16 \
 CONF_CC_OPTS_STAGE1='-Wl,--no-execute-only -Qunused-arguments' \
 CONF_CC_OPTS_STAGE2='-Wl,--no-execute-only -Qunused-arguments' \
		config_args='--with-ffi-includes=/usr/local/include 
			--with-ffi-libraries=/usr/local/lib 
			--with-gmp-includes=/usr/local/include 
			--with-gmp-libraries=/usr/local/lib 
			--with-iconv-includes=/usr/local/include 
			--with-iconv-libraries=/usr/local/lib 
			--with-system-libffi ' \
  ./validate --no-clean --fast "$@"
